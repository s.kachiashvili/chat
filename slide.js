$("document").ready(function() {
  $("#left_btn").click(function() {
    $("#lenta").animate({ left: "-=400px" }, 100, function() {
      $("#lenta div:last-child").prependTo("#lenta ");
      $("#lenta ").css("left", "");
    });
  });

  $("#right_btn").click(function() {
    $("#lenta").animate({ left: "+400px" }, 100, function() {
      $("#lenta div:first-child").appendTo("#lenta ");
      $("#lenta ").css("left", "");
    });
  });
});
